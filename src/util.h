#ifndef _UTIL_H_
#define _UTIL_H_
#include "mem_internals.h"

#include <stddef.h>

inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }
block_size two_merged_blocks_size
        (struct block_header const* restrict fst,
         struct block_header const* restrict snd);

_Noreturn void err( const char* msg, ... );
#endif
