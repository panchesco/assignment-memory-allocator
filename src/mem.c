#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region ( void const * addr, size_t query ) {
    /*  ??? */
    const size_t rsz = region_actual_size(query + offsetof(struct block_header, contents));
    void * region_addr = map_pages(addr, rsz, MAP_FIXED_NOREPLACE);
    if (region_addr == MAP_FAILED) {
        region_addr = map_pages(addr, rsz, 0);
        if (region_addr != MAP_FAILED) {
            const struct region region = (struct region) {.addr = region_addr, .size = rsz, false};
            block_init(region_addr, (block_size) {.bytes = rsz}, NULL);
            return region;
        }
        return REGION_INVALID;
    }
    const struct region region = (struct region) {.addr = region_addr, .size = rsz, true};
    block_init(region_addr, (block_size) {.bytes = rsz}, NULL);
    return region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
    const struct region region = alloc_region( HEAP_START, initial );
    if ( region_is_invalid(&region) ) return NULL;
    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
    return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header * block, size_t capacity_q ) {
    /*  ??? */
    if (block_splittable(block, capacity_q)) {
        block_size fst_bsz = size_from_capacity((block_capacity){.bytes = capacity_q});
        block_size snd_bsz = size_from_capacity((block_capacity){.bytes = block->capacity.bytes - fst_bsz.bytes});
        void * snd_addr = (void * ) (block->contents + capacity_q);
        struct block_header * next = block->next;
        block_init(block, fst_bsz, (struct block_header *) snd_addr);
        block_init((struct block_header *) snd_addr, snd_bsz, next);
        return true;
    }
    if (block->is_free == false) {
        return false;
    } else {
        block_init(block, size_from_capacity(block->capacity), block->next);
        return false;
    }
}

/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
    return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
        struct block_header const* fst,
        struct block_header const* snd ) {
    return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    /*  ??? */
    const struct block_header * const nxt = block->next;
    if (nxt == NULL) return false;
    if (mergeable(block, nxt)) {
        block_init(block, two_merged_blocks_size(block, nxt), nxt->next);
        return true;
    }
    return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
    struct block_header* block;
};


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz )    {
    /*??? */
    while (true) {
        if (try_merge_with_next(block)) {continue;}
        if (block_is_big_enough(sz, block) && block->is_free == true) {return (struct block_search_result) {BSR_FOUND_GOOD_BLOCK, block};}
        if (block->next == NULL) {break;}
        block = block->next;
    }
    return (struct block_search_result) {BSR_REACHED_END_NOT_FOUND, block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    const struct block_search_result search_result = find_good_or_last(block, query);
    if (search_result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(search_result.block, query);
        search_result.block->is_free = false;
    }
    return search_result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    /*  ??? */
    const void * region_addr = block_after(last);
    const struct region allocated_region = alloc_region(region_addr, query);
    if (!region_is_invalid(&allocated_region)) {
        last->next = (struct block_header *) allocated_region.addr;
        if (try_merge_with_next(last)) {
            return last;
        }
        return (struct block_header *) allocated_region.addr;
    }
    return NULL;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    /*  ??? */
    const size_t capacity_q = size_max(BLOCK_MIN_CAPACITY, query);
    struct block_search_result bsr = try_memalloc_existing(capacity_q, heap_start);
    if (bsr.type == BSR_REACHED_END_NOT_FOUND) {
        if (grow_heap(bsr.block, capacity_q) == NULL) return NULL;
        bsr = try_memalloc_existing(capacity_q, bsr.block);
        if (bsr.type != BSR_FOUND_GOOD_BLOCK) return NULL;
    }
    return bsr.block;
}

void * _malloc( size_t query) {
    struct block_header* const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void _free( void * mem ) {
    if (!mem) return ;
    struct block_header* header = block_get_header( mem );
    header->is_free = true;
    while (try_merge_with_next(header)) {}
    /*  ??? */
}
