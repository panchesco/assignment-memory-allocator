#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"


_Noreturn void err( const char* msg, ... ) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    abort();
}


extern inline size_t size_max( size_t x, size_t y );

block_size two_merged_blocks_size (struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return size_from_capacity((block_capacity) {.bytes = size_from_capacity(snd->capacity).bytes + fst->capacity.bytes});
}
